stages:
  - build
  - test
  - prepare
  - deploy

image: tmaier/docker-compose

variables:
  # this variable enables caching withing docker-in-docker
  # https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-workflow-with-docker-executor
  MOUNT_POINT: /builds/$CI_PROJECT_PATH/vendor
  # bundler-specific
  BUNDLE_JOBS: 4
  BUNDLE_PATH: vendor

.test_settings: &test_settings
  variables:
    BUNDLE_WITHOUT: website:assorted
  stage: test
  services:
    - docker:dind
  except:
    - blog
  artifacts:
    paths:
      - coverage/

.jit_matrix: &jit_matrix
  parallel:
    matrix:
      - RUBYOPT: ["", "--jit --jit-warnings --jit-wait"]
# Cache gems in between builds
cache:
  key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
  paths:
    - vendor

test jruby:
  <<: *test_settings
  script:
    ./spec.sh jruby 9.0.0.0
  allow_failure: true
test ruby 2/1:
  <<: *test_settings
  script:
    ./spec.sh ruby 2.1
test ruby 2/2:
  <<: *test_settings
  only:
    - master
  script:
    ./spec.sh ruby 2.2
test ruby 2/3:
  <<: *test_settings
  script:
    ./spec.sh ruby 2.3
test ruby 2/4:
  <<: *test_settings
  only:
    - master
  script:
    ./spec.sh ruby 2.4
test ruby 2/5:
  <<: *test_settings
  only:
    - master
  script:
    ./spec.sh ruby 2.5
test ruby 2/6:
  <<: *test_settings
  <<: *jit_matrix
  only:
    - master
  script:
    ./spec.sh ruby 2.6
test ruby 2/7:
  <<: *test_settings
  <<: *jit_matrix
  script:
    ./spec.sh ruby 2.7
test ruby 3/0:
  <<: *test_settings
  <<: *jit_matrix
  script:
    ./spec.sh ruby 3.0
test truffleruby:
  <<: *test_settings
  script:
    ./spec.sh truffleruby latest
  allow_failure: true

jekyll:
  stage: test
  image: "ruby:2.7-alpine"
  variables:
    JEKYLL_ENV: production
    BUNDLE_WITHOUT: test:coverage:assorted
  script:
    - apk --update add g++ make git
    - bundle install --quiet
    - bundle exec rake prepare_website
    - mv rdoc/ www
    - mv wiki/ www
    - bundle exec jekyll build -s www -d public
  artifacts:
    paths:
      - public/
  only:
    - master
    - blog

coverage:
  coverage: '/\(\d+.\d+\%\) covered/'
  stage: prepare
  variables:
    BUNDLE_WITHOUT: test:website:assorted
  except:
    - blog

  image: "ruby:3.0-alpine"
  script:
    - apk --update add g++ make git
    - bundle install --quiet
    # this is a workaround, because simplecov doesn't support relative paths.
    #
    # https://github.com/simplecov-ruby/simplecov/issues/887
    #
    # this is necessary in our case, because coverage files are generated inside
    # the custom docker container, which runs in /home, and the absolute paths aren't
    # gonna match the paths where the files are, once we merge them. Therefore,
    # all of those have to be rewritten before the merge happens.
    #
    - find coverage -name "*resultset.json" -exec sed -i 's?/home?'`pwd`'?' {} \;
    - bundle exec rake coverage:report
  artifacts:
    paths:
      - "coverage/"

pages:
  stage: deploy
  needs:
    - coverage
    - jekyll
  script:
    - "if [ -d coverage ] ; then mv coverage public ; fi"
  artifacts:
    paths:
      - public
    expire_in: 30 days
  only:
    - master
    - blog
