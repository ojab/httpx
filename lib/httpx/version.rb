# frozen_string_literal: true

module HTTPX
  VERSION = "0.15.3"
end
