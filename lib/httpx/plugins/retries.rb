# frozen_string_literal: true

module HTTPX
  module Plugins
    #
    # This plugin adds support for retrying requests when certain errors happen.
    #
    # https://gitlab.com/honeyryderchuck/httpx/wikis/Retries
    #
    module Retries
      MAX_RETRIES = 3
      # TODO: pass max_retries in a configure/load block

      IDEMPOTENT_METHODS = %i[get options head put delete].freeze
      RETRYABLE_ERRORS = [IOError,
                          EOFError,
                          Errno::ECONNRESET,
                          Errno::ECONNABORTED,
                          Errno::EPIPE,
                          (TLSError if defined?(TLSError)),
                          TimeoutError,
                          Parser::Error,
                          Errno::EINVAL,
                          Errno::ETIMEDOUT].freeze

      def self.extra_options(options)
        Class.new(options.class) do
          def_option(:retry_after, <<-OUT)
            # return early if callable
            unless value.respond_to?(:call)
              value = Integer(value)
              raise TypeError, ":retry_after must be positive" unless value.positive?
            end

            value
          OUT

          def_option(:max_retries, <<-OUT)
            num = Integer(value)
            raise TypeError, ":max_retries must be positive" unless num.positive?

            num
          OUT

          def_option(:retry_change_requests)

          def_option(:retry_on, <<-OUT)
            raise ":retry_on must be called with the response" unless value.respond_to?(:call)

            value
          OUT
        end.new(options).merge(max_retries: MAX_RETRIES)
      end

      module InstanceMethods
        def max_retries(n)
          with(max_retries: n.to_i)
        end

        private

        def fetch_response(request, connections, options)
          response = super

          if response &&
             request.retries.positive? &&
             __repeatable_request?(request, options) &&
             (
               # rubocop:disable Style/MultilineTernaryOperator
               options.retry_on ?
               options.retry_on.call(response) :
               (
                 response.is_a?(ErrorResponse) && __retryable_error?(response.error)
               )
               # rubocop:enable Style/MultilineTernaryOperator
             )
            response.close if response.respond_to?(:close)
            request.retries -= 1
            log { "failed to get response, #{request.retries} tries to go..." }
            request.transition(:idle)

            retry_after = options.retry_after
            retry_after = retry_after.call(request, response) if retry_after.respond_to?(:call)

            if retry_after

              log { "retrying after #{retry_after} secs..." }
              pool.after(retry_after) do
                log { "retrying!!" }
                connection = find_connection(request, connections, options)
                connection.send(request)
              end
            else
              connection = find_connection(request, connections, options)
              connection.send(request)
            end

            return
          end
          response
        end

        def __repeatable_request?(request, options)
          IDEMPOTENT_METHODS.include?(request.verb) || options.retry_change_requests
        end

        def __retryable_error?(ex)
          RETRYABLE_ERRORS.any? { |klass| ex.is_a?(klass) }
        end
      end

      module RequestMethods
        attr_accessor :retries

        def initialize(*args)
          super
          @retries = @options.max_retries
        end
      end
    end
    register_plugin :retries, Retries
  end
end
